<?php
// include all dependencies
require_once __DIR__ . '/inc/index.php';
require_once __DIR__ . '/views.php';

use Kucrut\Vite;

function ex_format_date_to_string($dateString) {
	$year = substr($dateString, 0, 4);
	$month = substr($dateString, 4, 2);
	$day = substr($dateString, 6, 2);

	return "{$year}-{$month}-{$day}";
}

// add ex_global_obj to header
add_action( 'wp_enqueue_scripts', function (): void {
	Vite\enqueue_asset(
		__DIR__ . '/dist',
		'assets/main.js',
		[
			'handle' => 'my-script-handle',
			'dependencies' => ['jquery'], // Optional script dependencies. Defaults to empty array.
			'css-dependencies' => [], // Optional style dependencies. Defaults to empty array.
			'css-media' => 'all', // Optional.
			'css-only' => false, // Optional. Set to true to only load style assets in production mode.
			'in-footer' => true, // Optional. Defaults to false.
		]
	);
	// pass js object to 'my-script-handle'
	wp_localize_script( 'my-script-handle', 'ex_global_obj', [
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'home_url' => home_url(),
	]);
} );

add_action( 'wp_enqueue_scripts', function() {
	$random = rand( 1, 1000000 );
  
	  // Enqueue your files on the canvas & frontend, not the builder panel. Otherwise custom CSS might affect builder)
	  if ( ! bricks_is_builder_main() ) {
		  wp_enqueue_style( 'bricks-child', get_stylesheet_uri(), ['bricks-frontend'], filemtime( get_stylesheet_directory() . '/style.css' ) );
	  }
  
	// add script
	wp_enqueue_script( 'jquery-modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', array( 'jquery' ), $random, true );
	// wp_enqueue_style( 'jquery-modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css', array(), $random, 'all' );
  } );

  /**
 * Add text strings to builder
 */
add_filter( 'bricks/builder/i18n', function( $i18n ) {
	// For element category 'custom'
	$i18n['custom'] = esc_html__( 'Custom', 'bricks' );
  
	return $i18n;
  } );
  
// 메뉴를 등록하는 함수
function ex_menu_register() {
register_nav_menus(array(
	'primary' => 'Primary Menu',
));
}
add_action('after_setup_theme', 'ex_menu_register');

function 기수_도시_가져오기($content, $city_meta_key) {
	$uid = $content->uid;
	$user_id = $content->getUserID();
	$meta_key = 'user_group';
	$기수 = get_user_meta($user_id, $meta_key, true);
	$city = $content->getOptionValue($city_meta_key);
	return [$기수, $city];
}

// kboard date filter
add_filter( 'kboard_content_date', function($date, $content, $board) {
	$_date = $content->row->date;
	$date = ex_format_date_to_string($_date);	
	return $date;
}, 9999, 3 );

// kboard 작성자 필터
function ex_kboard_author_filter($user_display, $user_id, $user_name, $type, $builder) {
	$user_info = get_userdata($user_id);
	$first_name = $user_info->first_name;
	$last_name = $user_info->last_name;
	$full_name = $first_name . $last_name;

	return $full_name;
}
add_filter('kboard_user_display', 'ex_kboard_author_filter', 10, 5);

// Function to slice and append '...' to a title
function ex_slice_title($title, $max_length = 40) {
    // Check if the title is set and not empty
    if (!empty($title)) {
        // Check if the title length is greater than the maximum length
        if (strlen($title) > $max_length) {
            // If yes, slice the title and append '...'
            return substr($title, 0, $max_length) . '...';
        } else {
            // If no, just return the title as it is
            return $title;
        }
    } else {
        // If the title is not set or empty, return a default message or handle the error
        return 'Title not available';
    }
}

function hide_admin_bar_for_logged_in_users() {
	// except adnimistrator
    if (is_user_logged_in() && !current_user_can('administrator')) {
        add_filter('show_admin_bar', '__return_false');
    }
}
add_action('after_setup_theme', 'hide_admin_bar_for_logged_in_users');

// 관리자로 로그인한 경우 #brx-header와 #brx-content 의 padding-top: 32px;
add_action( 'wp_head', function() {
	if (current_user_can('administrator')) {
		?>
		<style>
			body #brx-header {
				padding-top: 46px !important;
			}
			body #brx-content {
				padding-top: calc(110px + 32px) !important;
			}
			@media screen and (max-width: 600px) {
				body #brx-content {
					padding-top: calc(46px) !important;
				}
				body .offcanvas-nav-container {
					padding-top: 42px;
				}
			}
		</style>
		<?php
	}
} );