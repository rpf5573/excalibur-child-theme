<?php

class Offcanvas_Nav_Walker extends Walker_Nav_Menu {
    // 서브메뉴 시작 태그
    public function start_lvl(&$output, $depth = 0, $args = null) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class='sub-menu'>\n";
    }

    // 메뉴 항목 시작 태그
    public function start_el(&$output, $item, $depth = 0, $args = null, $id = 0) {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $class_names = '';
        $value = '';
        $icon_html = '';

        // 서브메뉴가 있는 경우 아이콘 URL 설정
        if ($args->walker->has_children) {
            $down_icon_url = WPM_Helpers::get_icon_url('chevron-down.svg');
            $up_icon_url = WPM_Helpers::get_icon_url('chevron-up.svg');
            $icon_html = "<span class='toggle-sub-menu'><img src='{$down_icon_url}' alt='expand' class='svg-icon svg-icon-down' />";
            $icon_html .= "<img src='{$up_icon_url}' alt='collapse' class='svg-icon svg-icon-up' style='display:none;' /></span>";
            $class_names = 'has-sub-menu';
        }

        $item_classes = $item->classes; // this is array, and merge it into a string
        $item_classes = implode(' ', $item_classes);

        $output .= "$indent<li class='menu-item $class_names $item_classes'>";
        $output .= '<a href="' . esc_url($item->url) . '">' . esc_html($item->title) . $icon_html . '</a>';
    }
}
