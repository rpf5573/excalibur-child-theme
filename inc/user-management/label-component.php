<div class="label-container tw-w-[120px] tw-flex tw-justify-center tw-items-center tw-min-h-[60px] tw-bg-purple-100 tw-h-full md:tw-w-[80px] <?php echo isset($args['additional_class']) ? $args['additional_class'] : ''; ?>">
    <label for="<?php echo isset($args['input_id']) ? $args['input_id'] : ''; ?>" class="tw-block tw-text-center tw-text-[16px] tw-text-black md:tw-text-[12px]">
        <b><?php echo isset($args['label_text']) ? $args['label_text'] : ''; ?></b>
    </label>
</div>
