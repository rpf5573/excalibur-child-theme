<div class="tw-flex tw-gap-5 address-component">
    <!-- 라벨 컴포넌트 사용 -->
    <div class="tw-w-[120px] tw-flex tw-justify-center tw-items-center tw-label-container md:tw-w-[80px]">
        <?php get_template_part('inc/user-management/label-component', null, array('input_id' => $args['input_id'], 'label_text' => $args['label_text'], 'additional_class' => 'tw-h-full')); ?>
    </div>
    <div class="tw-flex-1 tw-py-3">
        <!-- 우편번호 필드와 버튼 -->
        <div class="tw-mb-2">
            <div class="tw-flex tw-gap-3 md:tw-flex-col">
                <input type="text" id="<?php echo $args['postcode_id']; ?>" name="<?php echo $args['postcode_name']; ?>" value="<?php echo $args['postcode_value']; ?>" placeholder="" class="tw-w-full tw-px-3 tw-py-2 tw-border tw-rounded tw-mr-2 tw-flex-1 tw-max-w-[150px] postcode-input md:tw-max-w-full md:tw-w-full">
                <button type="button" class="postcode-search-button tw-bg-gray-600 tw-hover:tw-bg-gray-400 tw-text-white tw-font-bold tw-py-2 tw-px-4 tw-rounded md:tw-h-[42px] md:tw-max-w-[140px] md:tw-text-center">
                    <span class="text-little-up">우편번호 찾기</span>
                </button>
            </div>
        </div>
        <!-- 주소 필드 1 -->
        <div class="tw-mb-2">
            <input type="text" id="<?php echo $args['address1_id']; ?>" name="<?php echo $args['address1_name']; ?>" value="<?php echo $args['address1_value']; ?>" placeholder="" class="tw-w-full tw-px-3 tw-py-2 tw-border tw-rounded address1-input">
        </div>
        <!-- 주소 필드 2 -->
        <div>
            <input type="text" id="<?php echo $args['address2_id']; ?>" name="<?php echo $args['address2_name']; ?>" value="<?php echo $args['address2_value']; ?>" placeholder="" class="tw-w-full tw-px-3 tw-py-2 tw-border tw-rounded address2-input">
        </div>
    </div>
</div>
