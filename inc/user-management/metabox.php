<?php
add_action( 'rwmb_meta_boxes', function( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'  => '기본정보',
        'type'   => 'user',

        'fields' => array(
            array(
                'name' => '과정명',
                'id'   => 'course_name',
                'type' => 'text',
            ),
            array(
                'name' => '입학일자',
                'id'   => 'admission_date',
                'type' => 'date',
            ),
            array(
                'name' => '기수',
                'id'   => 'batch',
                'type' => 'text',
            ),
            array(
                'name' => '수료일자',
                'id'   => 'completion_date',
                'type' => 'date',
            ),
            array(
                'name' => '휴대전화',
                'id'   => 'mobile_phone',
                'type' => 'text',
            ),
            array(
                'name' => '자택주소1',
                'id'   => 'home_address_1',
                'type' => 'text',
            ),
            array(
                'name' => '자택주소2',
                'id'   => 'home_address_2',
                'type' => 'text',
            ),
            array(
                'name' => '자택 우편번호',
                'id'   => 'home_postcode',
                'type' => 'text',
            ),
            array(
                'name' => '우편물수령지',
                'id'   => 'mailing_address',
                'type'    => 'radio',
                'inline'  => true,
                'options' => [
                    'home' => '집',
                    'work' => '직장',
                    'email' => '이메일',
                ],
            ),
        ),
    );

    $meta_boxes[] = array(
        'title'  => '직장정보',
        'type'   => 'user',

        'fields' => array(
            array(
                'name' => '프로필 이미지',
                'id'   => 'profile_image_url',
                'type' => 'file_input',
            ),
            array(
                'name' => '직장명',
                'id'   => 'workplace_name',
                'type' => 'text',
            ),
            array(
                'name' => '직위',
                'id'   => 'position',
                'type' => 'text',
            ),
            array(
                'name' => '직장주소1',
                'id'   => 'work_address_1',
                'type' => 'text',
            ),
            array(
                'name' => '직장주소2',
                'id'   => 'work_address_2',
                'type' => 'text',
            ),
            array(
                'name' => '직장 우편번호',
                'id'   => 'work_postcode',
                'type' => 'text',
            ),
            array(
                'name' => '집전화 지역번호',
                'id'   => 'home_phone_1',
                'type' => 'text',
            ),
            array(
                'name' => '집전화',
                'id'   => 'home_phone_2',
                'type' => 'text',
            ),
            array(
                'name' => '부서명',
                'id'   => 'department',
                'type' => 'text',
            ),
            array(
                'name' => '직장전화 지역번호',
                'id'   => 'work_phone_1',
                'type' => 'text',
            ),
            array(
                'name' => '직장전화',
                'id'   => 'work_phone_2',
                'type' => 'text',
            ),
        ),
    );

    return $meta_boxes;
} );