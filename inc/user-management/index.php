<?php
require_once __DIR__ . '/api.php';
require_once __DIR__ . '/metabox.php';

// My Account Shortcode
add_shortcode( 'ex_my_account', function() {
    ob_start();
    $user_id = get_current_user_id();
    // check if the user is logged in
    if ( ! $user_id ) {
        return '<p>You must be logged in to view this page.</p>';
    }

    $fields = [
        'profile_image_url', 'course_name', 'admission_date', 'batch', 'completion_date',
        'mobile_phone', 'home_address_1', 'home_address_2', 'home_postcode', 'mailing_address',
        'workplace_name', 'position', 'work_address_1', 'work_address_2', 'work_postcode',
        'native_password', 'home_phone_1', 'home_phone_2', 'department', 'work_phone_1', 'work_phone_2'
    ];

    $field_values = [];
    foreach ($fields as $field) {
        $field_values[$field] = WPM_Helpers::mb_get_user_meta([
            'user_id' => $user_id,
            'field_id' => $field
        ]);
    }

    $no_profile_image = !isset($field_values['profile_image_url']) || empty($field_values['profile_image_url']);

    if ($no_profile_image) {
        $field_values['profile_image_url'] = '';
    }

    // 몇몇 정보들은 워드프레스 기본 필드에서 가져옵니다
    // get user email
    $field_values['email'] = get_userdata($user_id)->user_email;

    // get user's first name and last name and insert it into full_name field
    $user_info = get_userdata($user_id);
    $field_values['full_name'] = $user_info->last_name . $user_info->first_name;

    $mobile_phone_1 = '010';
    $mobile_phone_2 = '0000';
    $mobile_phone_3 = '0000';
    if (isset($field_values['mobile_phone']) && !empty($field_values['mobile_phone'])) {
        $mobile_phone = $field_values['mobile_phone'];
        $mobile_phone_1 = substr($mobile_phone, 0, 3);
        $mobile_phone_2 = substr($mobile_phone, 3, 4);
        $mobile_phone_3 = substr($mobile_phone, 7, 4);
    }

    $home_phone_1 = '02';
    $home_phone_2 = '0000000';
    if (isset($field_values['home_phone_1']) && !empty($field_values['home_phone_1'])) {
        $home_phone_1 = $field_values['home_phone_1'];
    }
    if (isset($field_values['home_phone_2']) && !empty($field_values['home_phone_2'])) {
        $home_phone_2 = $field_values['home_phone_2'];
    }

    $work_phone_1 = '02';
    $work_phone_2 = '0000000';
    if (isset($field_values['work_phone_1']) && !empty($field_values['work_phone_1'])) {
        $work_phone_1 = $field_values['work_phone_1'];
    }
    if (isset($field_values['work_phone_2']) && !empty($field_values['work_phone_2'])) {
        $work_phone_2 = $field_values['work_phone_2'];
    }
    ?>
    <form id="my_account_form" class="tw-flex tw-flex-col tw-gap-12">
        <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
        <?php wp_nonce_field( 'my_account_action', 'my_account_nonce' ); ?>

        <div class="base-inf">
            <h2 class="tw-font-bold tw-pb-3 tw-text-3xl">기본정보</h2>
            <div class="tw-mx-auto tw-border-t-2 tw-border-gray-400 tw-border-b">
                <div class="tw-flex tw-flex-col">

                    <!-- 프로필 이미지 -->
                    <div class="tw-flex tw-flex-col tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'profile_image', 'label_text' => '프로필 이미지')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center">
                                <div class="tw-flex tw-flex-col tw-ml-5 tw-py-3">
                                    <div id="profile_image_placeholder" class="<?php echo $no_profile_image ? '' : 'hidden'; ?>">
                                        <button type="button" class="profile_image_upload_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-camera"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                                            <span class="tw-text-2xl">Add photo</span>
                                        </button>
                                    </div>
                                    <div class="profile_image_preview_container <?php echo $no_profile_image ? 'hidden' : ''; ?>">
                                        <button type="button" class="remove_profile_image_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                        </button>
                                        <img id="profile_image_preview" src="<?php echo $field_values['profile_image_url']; ?>" alt="프로필 사진" class="tw-w-full tw-h-full tw-mb-2 tw-block">
                                    </div>
                                    <input type="file" id="profile_image" name="profile_image" accept="image/*" class="!tw-text-xl">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 이름: 전체 행 -->
                    <div class="tw-flex tw-flex-col tw-gap-6 tw-border-b tw-border-gray-200">
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'full_name', 'label_text' => '이름')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'full_name', 'input_name' => 'full_name', 'value' => $field_values['full_name'])); ?>
                                <span><?php echo $field_values['full_name']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 과정명, 기수: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'course_name', 'label_text' => '과정명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'course_name', 'input_name' => 'course_name', 'value' => $field_values['course_name'])); ?>
                                <span><?php echo $field_values['course_name']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'batch', 'label_text' => '기수')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'batch', 'input_name' => 'batch', 'value' => $field_values['batch'])); ?>
                                <span><?php echo $field_values['batch']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 입학일자, 수료일자: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'admission_date', 'label_text' => '입학일자')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'date', 'input_id' => 'admission_date', 'input_name' => 'admission_date', 'value' => $field_values['admission_date'])); ?>
                                <span><?php echo $field_values['admission_date']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'completion_date', 'label_text' => '수료일자')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'date', 'input_id' => 'completion_date', 'input_name' => 'completion_date', 'value' => $field_values['completion_date'])); ?>
                                <span><?php echo $field_values['completion_date']; ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- 이메일, 비밀번호: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'email', 'label_text' => '이메일<br/>(로그인 ID)')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php // get_template_part('inc/user-management/input-component', null, array('input_type' => 'email', 'input_id' => 'email', 'input_name' => 'email', 'value' => $field_values['email'])); ?>
                                <span><?php echo $field_values['email']; ?></span>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'native_password', 'label_text' => '비밀번호')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5 tw-gap-6 tw-py-3 md:tw-flex-col md:tw-items-start">
                                <div class="left">
                                    <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'native_password', 'input_id' => 'native_password', 'input_name' => 'native_password', 'input_type' => 'password')); ?>
                                    <div>
                                        <span class="tw-text-xl tw-text-black">(8~16자의 영문 소문자와 숫자의 혼용된<br class="md:tw-hidden" /> 조합이어야 함)</span>
                                    </div>
                                </div>
                                <div class="right">
                                    <!-- 비밀번호 표시 toggle -->
                                    <div class="toggle-password tw-flex tw-gap-2">
                                        <input type="checkbox" id="toggle-password">
                                        <label for="toggle-password" class="tw-flex tw-items-center tw-cursor-pointer">
                                            <span class="tw-mr-2 tw-text-2xl">비밀번호 표시</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 휴대전화, 집전화: 반쪽-반쪽 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'mobile_phone', 'label_text' => '휴대전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-gap-2 tw-px-5 mobile_phone-container">
                                <div class="tw-max-w-[60px]">
                                    <input type="text" id="mobile_phone_1" name="mobile_phone_1" value="<?php echo $mobile_phone_1; ?>" />
                                </div>
                                <span>-</span>
                                <div class="tw-max-w-[80px]">
                                    <input type="text" id="mobile_phone_2" name="mobile_phone_2" value="<?php echo $mobile_phone_2; ?>" />
                                </div>
                                <span>-</span>
                                <div class="tw-max-w-[80px]">
                                    <input type="text" id="mobile_phone_3" name="mobile_phone_3" value="<?php echo $mobile_phone_3; ?>" />
                                </div>
                                <?php
                                // get_template_part('inc/user-management/input-component', null, array('input_type' => 'number', 'input_id' => 'mobile_phone', 'input_name' => 'mobile_phone', 'value' => $field_values['mobile_phone']));
                                ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_type' => 'number', 'input_id' => 'home_phone', 'label_text' => '집전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5 tw-gap-2">
                                <div class="tw-w-[80px]">
                                    <select name="home_phone_1" id="home_phone_1"> <?php
                                        $phonePrefixes = ["02", "031", "032", "051", "033", "041", "042", "043", "044", "052", "053", "054", "055", "061", "062", "063", "064"];
                                        $selectedPrefix = $home_phone_1;
                                        foreach ($phonePrefixes as $prefix) {
                                            echo '<option value="' . $prefix . '"' . ($prefix == $selectedPrefix ? ' selected' : '') . '>' . $prefix . '</option>';
                                        } ?>
                                    </select>
                                </div>
                                <span>-</span>
                                <div class="tw-w-[116px]">
                                    <input type="text" id="home_phone_2" name="home_phone_2" value="<?php echo $home_phone_2; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tw-border-b tw-border-gray-200">
                        <!-- 자택주소 -->
                        <?php get_template_part('inc/user-management/address-component', null, array(
                            'label_text' => '자택주소',
                            'input_id' => 'home_address',
                            'postcode_id' => 'home_postcode',
                            'postcode_name' => 'home_postcode',
                            'postcode_value' => $field_values['home_postcode'],
                            'address1_id' => 'home_address_1',
                            'address1_name' => 'home_address_1',
                            'address1_value' => $field_values['home_address_1'],
                            'address2_id' => 'home_address_2',
                            'address2_name' => 'home_address_2',
                            'address2_value' => $field_values['home_address_2'],
                        )); ?>
                    </div>

                    <!-- 우편물수령지 -->
                    <div class="tw-flex tw-gap-6 tw-items-center">
                        <div class="tw-w-[120px] tw-flex tw-justify-center tw-items-center md:tw-w-[80px]">
                            <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'mailing_address', 'label_text' => '우편물 수령지', 'additional_class' => 'tw-h-full')); ?>
                        </div>
                        <div class="tw-flex-1 tw-flex tw-gap-6">
                            <!-- 자택 -->
                            <div class="tw-flex tw-justify-center tw-items-center md:tw-flex-col">
                                <input type="radio" id="mailing_address_home" name="mailing_address" value="home" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'home'); ?>>
                                <label for="mailing_address_home" class="tw-mb-0 tw-text-[14px]">자택</label>
                            </div>
                            <!-- 직장 -->
                            <div class="tw-flex tw-justify-center tw-items-center md:tw-flex-col">
                                <input type="radio" id="mailing_address_work" name="mailing_address" value="work" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'work'); ?>>
                                <label for="mailing_address_work" class="tw-mb-0 tw-text-[14px]">직장</label>
                            </div>
                            <!-- 온라인 -->
                            <div class="tw-flex tw-justify-center tw-items-center md:tw-flex-col">
                                <input type="radio" id="mailing_address_email" name="mailing_address" value="email" class="tw-mr-2" <?php checked($field_values['mailing_address'], 'email'); ?>>
                                <label for="mailing_address_email" class="tw-mb-0 tw-text-[14px]">이메일</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="job-info">
            <h2 class="tw-font-bold tw-pb-3 tw-text-3xl">직장정보</h2>
            <div class="tw-mx-auto tw-border-t-2 tw-border-gray-400 tw-border-b">
                <div class="tw-flex tw-flex-col">
                    <!-- 직장명, 부서명 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'workplace_name', 'label_text' => '직장명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'workplace_name', 'input_name' => 'workplace_name', 'value' => $field_values['workplace_name'])); ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'department', 'label_text' => '부서명')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'department', 'input_name' => 'department', 'value' => $field_values['department'])); ?>
                            </div>
                        </div>
                    </div>

                    <!-- 직위, 직장전화 -->
                    <div class="tw-flex tw-gap-6 tw-border-b tw-border-gray-200 md:tw-flex-col md:tw-gap-0">
                        <div class="tw-flex tw-flex-1 md:tw-border-b md:tw-border-gray">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'position', 'label_text' => '직위')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <?php get_template_part('inc/user-management/input-component', null, array('input_type' => 'text', 'input_id' => 'position', 'input_name' => 'position', 'value' => $field_values['position'])); ?>
                            </div>
                        </div>
                        <div class="tw-flex tw-flex-1">
                            <div>
                                <?php get_template_part('inc/user-management/label-component', null, array('input_id' => 'work_phone', 'label_text' => '직장전화')); ?>
                            </div>
                            <div class="tw-flex-1 tw-flex tw-justify-start tw-items-center tw-px-5">
                                <div class="tw-w-[80px]">
                                    <select name="work_phone_1" id="work_phone_1"> <?php
                                        $phonePrefixes = ["02", "031", "032", "051", "033", "041", "042", "043", "044", "052", "053", "054", "055", "061", "062", "063", "064"];
                                        $selectedPrefix = $work_phone_1;
                                        foreach ($phonePrefixes as $prefix) {
                                            echo '<option value="' . $prefix . '"' . ($prefix == $selectedPrefix ? ' selected' : '') . '>' . $prefix . '</option>';
                                        } ?>
                                    </select>
                                </div>
                                <span>-</span>
                                <div class="tw-w-[116px]">
                                    <input type="text" id="work_phone_2" name="work_phone_2" value="<?php echo $work_phone_2; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- 직장주소 -->
                    <?php get_template_part('inc/user-management/address-component', null, array(
                        'label_text' => '직장주소',
                        'input_id' => 'work_address',
                        'postcode_id' => 'work_postcode',
                        'postcode_name' => 'work_postcode',
                        'postcode_value' => $field_values['work_postcode'],
                        'address1_id' => 'work_address_1',
                        'address1_name' => 'work_address_1',
                        'address1_value' => $field_values['work_address_1'],
                        'address2_id' => 'work_address_2',
                        'address2_name' => 'work_address_2',
                        'address2_value' => $field_values['work_address_2'],
                    )); ?>
                </div>
            </div>
        </div>
        <div class="tw-flex tw-justify-center tw-py-4">
            <button type="submit" id="save_my_account" class="tw-bg-purple-900 tw-text-3xl tw-text-white tw-rounded-2xl tw-py-6 tw-px-12 tw-font-bold">
                <span class="tw-mt-[-2px] tw-block">저장</span>
            </button>
        </div>
    </form>
    <div class="loading-indicator hidden">
        <div class="inner">
            <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                    <div class="pswp__preloader__donut"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
    document.addEventListener('DOMContentLoaded', function() {
        const extractAddress = (data) => {
            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            const zonecode = data.zonecode;
            let addr = ''; // 주소 변수
            let extraAddr = ''; // 참고항목 변수

            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') {
                // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else {
                // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
            if (data.userSelectedType === 'R') {
                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                    extraAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if (data.buildingName !== '' && data.apartment === 'Y') {
                    extraAddr +=
                    extraAddr !== '' ? ', ' + data.buildingName : data.buildingName;
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if (extraAddr !== '') {
                    extraAddr = ' (' + extraAddr + ')';
                }
            }

            return {
                zonecode,
                address: addr + extraAddr,
            };
        };
        // Find all postcode search buttons
        const searchButtons = document.querySelectorAll('.postcode-search-button');
        searchButtons.forEach(function(button, index) {
            button.addEventListener('click', function() {
                new daum.Postcode({
                    oncomplete: function(data) {
                        const { zonecode, address } = extractAddress(data);
                        // Using index to target specific instances of the inputs
                        var container = button.closest('.address-component');
                        container.querySelector('.postcode-input').value = zonecode; // Postcode
                        container.querySelector('.address1-input').value = address; // Road address
                    }
                }).open();
            });
        });
    });
</script>
<?php
    return ob_get_clean();
} );

function ex_add_upload_files_to_all_roles() {
    global $wp_roles;
    if (!isset($wp_roles)) {
        $wp_roles = new WP_Roles();
    }

    foreach ($wp_roles->role_objects as $role) {
        if (!$role->has_cap('upload_files')) {
            $role->add_cap('upload_files', true);
            $role->add_cap('unfiltered_upload', true);
        }
    }
}
add_action('admin_init', 'ex_add_upload_files_to_all_roles');

add_action( 'edit_user_profile_update', function() {
    // when save user profile, and have new password, then save the new user password to 'native_password' metabox field
    if (isset($_POST['pass1']) && !empty($_POST['pass1'])) {
        $user_id = get_current_user_id();
        $value = $_POST['pass1'];
        $params = array(
            'user_id' => $user_id, // the ID of the user to update
            'field_id' => 'native_password',    // the ID of the field to update
            'value' => sanitize_text_field( $value ) // the new value for the field
        );
        WPM_Helpers::mb_set_user_meta( $params );
    }
} );