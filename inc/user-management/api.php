<?php

add_action( 'wp_ajax_save_my_account', 'ex_handle_save_my_account' );
function ex_handle_save_my_account() {
    ex_save_my_account_validate(); // validation first

    $user_id = get_current_user_id();
    
    // Handle the file upload for 'profile_image'
    if (isset($_FILES['profile_image']) && $_FILES['profile_image']['error'] === UPLOAD_ERR_OK) {
        require_once(ABSPATH . 'wp-admin/includes/admin.php');
        $file_return = wp_handle_upload($_FILES['profile_image'], array('test_form' => false));

        if (isset($file_return['error']) || isset($file_return['upload_error_handler'])) {
            wp_send_json_error('Error uploading file.');
            return;
        } else {
            $filename = $file_return['file'];
            $attachment = array(
                'post_mime_type' => $file_return['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => '',
                'post_status' => 'inherit',
                'guid' => $file_return['url']
            );

            $attachment_id = wp_insert_attachment($attachment, $file_return['file']);
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
            wp_update_attachment_metadata($attachment_id, $attachment_data);
            
            $params = [
                'user_id' => $user_id,
                'field_id' => 'profile_image_url',
                'value' => wp_get_attachment_url($attachment_id)
            ];
            WPM_Helpers::mb_set_user_meta($params);
        }
    }

    // insert password
    if (isset($_POST['native_password']) && !empty($_POST['native_password'])) {
        $value = $_POST['native_password'];
        wp_set_password($value, $user_id);
    }

    // other fields, use loop
    $fields = [
        'mobile_phone', 'home_phone_1', 'home_phone_2', 'home_address_1', 'home_address_2', 'home_postcode', 'mailing_address',
        'workplace_name', 'department', 'position', 'work_phone_1', 'work_phone_2', 'work_address_1', 'work_address_2', 'work_postcode'
    ];
    foreach ($fields as $field) {
        $value = isset($_POST[$field]) && !empty($_POST[$field]) ? $_POST[$field] : '';
        $params = array(
            'user_id' => $user_id, // the ID of the user to update
            'field_id' => $field,    // the ID of the field to update
            'value' => sanitize_text_field( $value ) // the new value for the field
        );
        WPM_Helpers::mb_set_user_meta( $params );
    }

    // Return a response
    wp_send_json_success( 'Data saved successfully' );
}

function ex_save_my_account_validate() {
    // Check if the user is logged in and has the right capability
    if (!is_user_logged_in() || !current_user_can('upload_files')) {
        wp_send_json_error('Permission denied');
        return;
    }

    // Check for nonce for security
    $nonce = $_POST['my_account_nonce'];
    
    if ( ! wp_verify_nonce( $nonce, 'my_account_action' ) ) {
        wp_send_json_error( 'Nonce verification failed', 400 );
        return;
    }

    // First, check the password
    // Validate the password before proceeding
    // If 'native_password' is received, update the user password with it
    if (isset($_POST['native_password']) && !empty($_POST['native_password'])) {
        $password = $_POST['native_password'];
        if (strlen($password) < 8) {
            wp_send_json_error('비밀번호는 8자리 이상이어야 합니다.');
            return;
        }
        if (strlen($password) > 16) {
            wp_send_json_error('비밀번호는 16자리 이하이어야 합니다.');
            return;
        }

        // 영문 소문자와 숫자가 섞여있어야 합니다.
        if (!preg_match('/[a-z]/', $password) || !preg_match('/[0-9]/', $password)) {
            wp_send_json_error('비밀번호는 8~16자의 영문 소문자와 숫자로 이루어져야 합니다.');
            return;
        }
    }


    // mobile_phone
    if (isset($_POST['mobile_phone']) && !empty($_POST['mobile_phone'])) {
        $value = $_POST['mobile_phone'];
        if (!preg_match('/^01[0-9]{8,9}$/', $value)) {
            wp_send_json_error('휴대전화 번호는 01로 시작하는 10~11자리 숫자여야 합니다.');
            return;
        }
    }

    // home_postcode
    if (isset($_POST['home_postcode']) && !empty($_POST['home_postcode'])) {
        $value = $_POST['home_postcode'];
        if (!preg_match('/^[0-9]{5}$/', $value)) {
            wp_send_json_error('우편번호는 5자리 숫자여야 합니다.');
            return;
        }
    }

    // work_postcode
    if (isset($_POST['work_postcode']) && !empty($_POST['work_postcode'])) {
        $value = $_POST['work_postcode'];
        if (!preg_match('/^[0-9]{5}$/', $value)) {
            wp_send_json_error('우편번호는 5자리 숫자여야 합니다.');
            return;
        }
    }
}