<?php
class WPM_Helpers {
    // CSS 파일을 추가하는 함수
    public static function enqueue_style($handle, $src = '', $deps = array(), $ver = false, $media = 'all') {
        wp_enqueue_style($handle, $src, $deps, $ver, $media);
    }

    // JavaScript 파일을 추가하는 함수
    public static function enqueue_script($handle, $src = '', $deps = array(), $ver = false, $in_footer = false) {
        wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
    }

    // assets 폴더의 URL을 반환하는 함수
    public static function get_assets_url() {
        return get_stylesheet_directory_uri() . '/assets/';
    }

    // icons 폴더의 URL을 반환하는 함수
    public static function get_icon_url($resource_name) {
        return self::get_assets_url() . 'icons/' . $resource_name;
    }

    // images 폴더의 URL을 반환하는 함수
    public static function get_image_url($resource_name) {
        return self::get_assets_url() . 'images/' . $resource_name;
    }

    // js 폴더의 URL을 반환하는 함수
    public static function get_js_url() {
        return self::get_assets_url() . 'js/';
    }

    // css 폴더의 URL을 반환하는 함수
    public static function get_css_url() {
        return self::get_assets_url() . 'css/';
    }

	// JavaScript 핸들러에 PHP 변수를 넘기는 함수
    public static function localize_script($handle, $object_name, $data) {
        wp_localize_script($handle, $object_name, $data);
    }

    // 설정 페이지에서 필드 값을 가져오는 함수 (배열 파라미터 사용)
    public static function mb_get_from_setting_page($params) {
        $field_id = $params['field_id'];
        $setting_page_id = $params['setting_page_id'];
        return rwmb_meta($field_id, ['object_type' => 'setting'], $setting_page_id);
    }

    // 설정 페이지에 특정 값을 저장하는 함수 (배열 파라미터 사용)
    public static function mb_set_to_setting_page($params) {
        $setting_page_id = $params['setting_page_id'];
        $field_id = $params['field_id'];
        $value = $params['value'];
        rwmb_set_meta($setting_page_id, $field_id, $value, ['object_type' => 'setting']);
    }

    // 사용자 메타 값을 저장하는 함수 (배열 파라미터 사용)
    public static function mb_set_user_meta($params) {
        $user_id = $params['user_id'];
        $field_id = $params['field_id'];
        $value = $params['value'];
        rwmb_set_meta($user_id, $field_id, $value, ['object_type' => 'user']);
    }

    // 사용자로부터 메타 값을 가져오는 함수 (배열 파라미터 사용)
    public static function mb_get_user_meta($params) {
        $field_id = $params['field_id'];
        $user_id = $params['user_id'];
        return rwmb_meta($field_id, ['object_type' => 'user'], $user_id);
    }

    // 이미지의 전체 URL을 가져오는 함수 (배열 파라미터 사용)
    public static function mb_get_image_url($params) {
        $field_id = $params['field_id'];
        $setting_page_id = $params['setting_page_id'];
        $image_meta = rwmb_meta($field_id, ['object_type' => 'setting'], $setting_page_id);
        return isset($image_meta['full_url']) ? $image_meta['full_url'] : '';
    }
}