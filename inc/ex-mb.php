<?php

class EX_MB {
    const SETTING_PAGE_ID = 'ex-site-setting';
    const SITE_LOGO_ID = 'site-logo';
    const SITE_MOBILE_LOGO_ID = 'site-mobile-logo';
    const SITE_OFFCANVAS_LOGO_ID = 'site-offcanvas-logo';

    // 사이트 로고 URL을 가져오는 메서드
    public static function get_site_logo_url() {
        $params = array(
            'field_id' => self::SITE_LOGO_ID,
            'setting_page_id' => self::SETTING_PAGE_ID
        );
        return WPM_Helpers::mb_get_image_url($params);
    }

    // 모바일 사이트 로고 URL을 가져오는 메서드
    public static function get_site_mobile_logo_url() {
        $params = array(
            'field_id' => self::SITE_MOBILE_LOGO_ID,
            'setting_page_id' => self::SETTING_PAGE_ID
        );
        return WPM_Helpers::mb_get_image_url($params);
    }

    // 오프캔버스 사이트 로고 URL을 가져오는 메서드
    public static function get_site_offcanvas_logo_url() {
        $params = array(
            'field_id' => self::SITE_OFFCANVAS_LOGO_ID,
            'setting_page_id' => self::SETTING_PAGE_ID
        );
        return WPM_Helpers::mb_get_image_url($params);
    }
}
