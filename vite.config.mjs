import { readFileSync } from 'node:fs';
import { v4wp } from '@kucrut/vite-for-wp';
import { wp_scripts } from '@kucrut/vite-for-wp/plugins';

export default {
	plugins: [
		v4wp( {
			input: 'assets/main.js', // Optional, defaults to 'src/main.js'.
			outDir: 'dist', // Optional, defaults to 'dist'.
		},
		wp_scripts()),
	],
    server: {
		host: 'excalibur2.local',
		cors: true,
		https: {
			cert: readFileSync( '/Applications/MAMP/Library/OpenSSL/certs/excalibur2.local.crt' ),
			key: readFileSync( '/Applications/MAMP/Library/OpenSSL/certs/excalibur2.local.key' ),
		},
	},
	build: {
		rollupOptions: {
			external: ['jquery'],
		},
	}
};