module.exports = {
  prefix: "tw-",
  content: [
    // https://tailwindcss.com/docs/content-configuration
    "./*.php",
    "./inc/**/*.php",
    "./templates/**/*.php",
    "./safelist.txt",
    //'./**/*.php', // recursive search for *.php (be aware on every file change it will go even through /node_modules which can be slow, read doc)
  ],
  safelist: [
    "text-center",
    //{
    //  pattern: /text-(white|black)-(200|500|800)/
    //}
  ],
  theme: {
    extend: {
      colors: {
        purple: {
          100: "#f5f3ff",
          200: "#ebe7ff",
          300: "#d0c6ff",
          400: "#b5a5ff",
          500: "#9a84ff",
          600: "#8a74ff",
          700: "#6e5eff",
          800: "#5249ff",
          900: "#7030a0",
        },
        gray: {
          100: "#f5f5f5",
          200: "#eeeeee",
          300: "#e0e0e0",
          400: "#bdbdbd",
          500: "#9e9e9e",
          600: "#7F7F80",
          700: "#616161",
          800: "#424242",
          900: "#212121",
        },
      },
    },
    screens: {
      xl: { max: "1279px" },
      // => @media (max-width: 1279px) { ... }

      lg: { max: "990px" },
      // => @media (max-width: 990px) { ... }

      md: { max: "767px" },
      // => @media (max-width: 767px) { ... }

      sm: { max: "639px" },
      // => @media (max-width: 639px) { ... }
    },
  },
  plugins: [],
};
